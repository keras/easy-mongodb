package com.easy.mongodb.core.biz;

import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.util.Locale;

/**
 * @ProductName: easy-mongodb
 * @ProjectName: com.easy.mongodb.core
 * @Description: note
 * @Author: vapeshop
 * @Date: 2022/6/20 15:46
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 15:46
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class GeoPoint {
    private static final long serialVersionUID = 3583151228933783558L;
    private final double x;
    private final double y;

    public GeoPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public GeoPoint(String point) {
        Assert.isTrue(point.contains(","), "Source point must not be null!");
//        this.resetFromString(point);
        this.x = Double.valueOf(point.split(",")[0]);
        this.y = Double.valueOf(point.split(",")[1]);
    }

    public GeoPoint(GeoPoint point) {
        Assert.notNull(point, "Source point must not be null!");
        this.x = point.x;
        this.y = point.y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public int hashCode() {
        int result = 1;
        long temp = Double.doubleToLongBits(this.x);
        result = 31 * result + (int) (temp ^ temp >>> 32);
        temp = Double.doubleToLongBits(this.y);
        result = 31 * result + (int) (temp ^ temp >>> 32);
        return result;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof GeoPoint)) {
            return false;
        } else {
            GeoPoint other = (GeoPoint) obj;
            if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
                return false;
            } else {
                return Double.doubleToLongBits(this.y) == Double.doubleToLongBits(other.y);
            }
        }
    }

    public String toString() {
        return String.format(Locale.ENGLISH, "GeoPoint [x=%f, y=%f]", this.x, this.y);
    }
}
