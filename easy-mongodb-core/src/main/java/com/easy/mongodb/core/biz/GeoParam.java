package com.easy.mongodb.core.biz;

import com.mongodb.client.model.geojson.Geometry;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * ProjectName: easy-mongodb
 * Description: Geo相关参数
 * Author: vapeshop
 * Date: 2022/6/22 10:48:17
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/22 10:48:17
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
@Builder
public class GeoParam {
    /**
     * 是否在此区域内
     */
    private boolean isIn;
    /**
     * 字段名
     */
    private String field;
    /**
     * geoBoundingBox 左上点坐标
     */
    private GeoPoint topLeft;
    /**
     * geoBoundingBox 右下点坐标
     */
    private GeoPoint bottomRight;
    /**
     * 中心点坐标
     */
    private GeoPoint centralGeoPoint;
    /**
     * 距离 双精度类型
     */
    private Double distance;
    /**
     * 距离 单位
     */
    private DistanceUnit distanceUnit;
    /**
     * 距离 字符串类型
     */
    private String distanceStr;
    /**
     * 不规则坐标点列表
     */
    private List<GeoPoint> geoPoints;
    /**
     * 已被索引形状的索引id
     */
    private String indexedShapeId;
    /**
     * 图形
     */
    private Geometry geometry;
    /**
     * 图形关系
     */
    private ShapeRelation shapeRelation;
}
