package com.easy.mongodb.core.biz;

import lombok.Data;

/**
 * @ProjectName: easy-mongodb
 * @Description: 更新参数
 * @Author: vapeshop
 * @Date: 2022/6/20 10:09:39
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 10:09:39
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
public class UpdateParam {
    /**
     * 字段
     */
    private String field;
    /**
     * 值
     */
    private Object value;
}
