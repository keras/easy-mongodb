package com.easy.mongodb.core.config;

import com.easy.mongodb.common.enums.FieldStrategy;
import com.easy.mongodb.common.enums.ProcessIndexStrategyEnum;
import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import static com.easy.mongodb.common.constants.BaseMongoConstants.EMPTY_STR;

/**
 * @ProjectName: easy-mongodb
 * @Description: 全局置项
 * @Author: vapeshop
 * @Date: 2022/6/16 14:19:43
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/16 14:19:43
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
public class GlobalConfig {
    /**
     * whether to print dsl log 是否打印执行的dsl语句
     */
    private boolean printDsl = true;
    /**
     * process index mode Smoothly by default 索引处理模式 默认开启平滑模式
     */
    private ProcessIndexStrategyEnum processIndexMode = ProcessIndexStrategyEnum.SMOOTHLY;
    /**
     * process index blocking main thread true by default 异步处理索引是否阻塞主线程 默认阻塞
     */
    private boolean asyncProcessIndexBlocking = true;
    /**
     * is distributed environment true by default 是否分布式环境 默认为是
     */
    private boolean distributed = true;

    /**
     * mongodb config 数据库配置
     */
    @NestedConfigurationProperty
    private DbConfig dbConfig = new DbConfig();

    /**
     * mongodb config 数据库配置
     */
    @Data
    public static class DbConfig {
        /**
         * index prefix eg:daily_, 索引前缀 可缺省
         */
        private String tablePrefix = EMPTY_STR;
        /**
         * index prefix eg:daily_, 索引前缀 可缺省
         */
        private String database = EMPTY_STR;
        /**
         * enable underscore to camel case default false 是否开启下划线自动转驼峰 默认关闭
         */
        private boolean mapUnderscoreToCamelCase = false;
        /**
         * Field update strategy default nonNull 字段更新策略,默认非null
         */
        private FieldStrategy fieldStrategy = FieldStrategy.NOT_NULL;
    }
}
