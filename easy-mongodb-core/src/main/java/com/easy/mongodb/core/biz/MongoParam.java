package com.easy.mongodb.core.biz;

import lombok.Data;
import org.bson.BsonDocument;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: easy-mongodb
 * @Description: 基本参数
 * @Author: vapeshop
 * @Date: 2022/6/20 10:07:55
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 10:07:55
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
public class MongoParam {
    /**
     * 是否开启管道聚合
     */
    private boolean enablePipeline = false;
    /**
     * mongodb 执行db.xxx.xxxx(参数)
     */
    private List<Bson> paramList = new ArrayList<>();
    /**
     * mongodb 执行db.xxx.xxxx().projection(参数).sort().limit().skip()
     */
    private Bson projection = new BsonDocument();
    /**
     * mongodb 执行db.xxx.xxxx().projection(参数).sort().limit().skip()
     */
    private Bson sort = new BsonDocument();
    /**
     * mongodb 执行db.xxx.xxxx().projection(参数).sort().limit().skip()
     */
    private Integer limit = 0;
    /**
     * mongodb 执行db.xxx.xxxx().projection(参数).sort().limit().skip()
     */
    private Integer skip = 0;
}
