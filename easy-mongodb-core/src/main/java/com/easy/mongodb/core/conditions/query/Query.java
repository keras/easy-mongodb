package com.easy.mongodb.core.conditions.query;

import com.easy.mongodb.core.biz.TableFieldInfo;
import com.easy.mongodb.core.toolkit.FieldUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Predicate;

/**
 * ProjectName: easy-mongodb
 * Description: 查询封装
 * Author: vapeshop
 * Date: 2022/7/12 15:21:35
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:21:35
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface Query<Children, T, R> extends Serializable {
    /**
     * 从第几条数据开始查询
     *
     * @param from 起始
     * @return 泛型
     */

    Children from(Integer from);

    /**
     * 总共查询多少条数据
     *
     * @param size 查询多少条
     * @return 泛型
     */
    Children size(Integer size);

    /**
     * 兼容MySQL语法 作用同size
     *
     * @param n 查询条数
     * @return 泛型
     */
    Children limit(Integer n);

    /**
     * 兼容MySQL语法 作用同from+size
     *
     * @param m offset偏移量,从第几条开始取,作用同from
     * @param n 查询条数,作用同size
     * @return 泛型
     */
    Children limit(Integer m, Integer n);

    default Children select(R column) {
        return select(FieldUtils.getFieldNameNotConvertId(column));
    }

    default Children select(R... columns) {
        return select(Arrays.stream(columns).map(FieldUtils::getFieldNameNotConvertId).toArray(String[]::new));
    }


    /**
     * 设置查询字段
     *
     * @param columns 查询列,支持多字段
     * @return 泛型
     */
    Children select(String... columns);

    /**
     * 查询字段
     *
     * @param predicate 预言
     * @return 泛型
     */
    Children select(Predicate<TableFieldInfo> predicate);

    /**
     * 过滤查询的字段信息(主键除外!)
     *
     * @param entityClass 实体类
     * @param predicate   预言
     * @return 泛型
     */
    Children select(Class<T> entityClass, Predicate<TableFieldInfo> predicate);
}
