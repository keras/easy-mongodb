package com.easy.mongodb.core.biz;

import com.easy.mongodb.common.annotation.JoinCondition;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * @author don
 */
@Getter
@AllArgsConstructor
public class JoinConditionDescription {
    /**
     * {@link JoinCondition#selfField()}
     */
    protected final String selfField;
    /**
     * {@link JoinCondition#joinField()}
     */
    protected final String joinField;
    /**
     * 中间表Entity，需要对应创建其Mapper
     */
    private final Class<?> midEntity;
    /**
     * {@link JoinCondition#selfMidField()}
     */
    private final String selfMidField;
    /**
     * {@link JoinCondition#joinMidField()}
     */
    private final String joinMidField;
    /**
     * 被关联的Entity，不再需要显示的指明，默认取字段上的声明类型
     */
    private final Class<?> entity;
    /**
     * 被关联的Entity，不再需要显示的指明，默认取字段上的声明类型
     */
    private final String field;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JoinConditionDescription condition = (JoinConditionDescription) o;
        return this.selfField.equals(condition.selfField) && this.joinField.equals(condition.joinField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.selfField, this.joinField);
    }

    @Override
    public String toString() {
        return this.selfField + "|" + this.joinField + "|" + this.field + "|" + this.selfMidField + "|" + this.joinMidField + "|" + this.entity + "|" + this.midEntity;
    }
}
