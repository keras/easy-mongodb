package com.easy.mongodb.core.conditions;

import com.easy.mongodb.common.params.SFunction;

/**
 * ProjectName: easy-mongodb
 * Description: Lambda 语法使用 Wrapper
 * Author: vapeshop
 * Date: 2022/7/8 11:02:52
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/8 11:02:52
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@SuppressWarnings("serial")
public abstract class AbstractLambdaWrapper<T, Children extends AbstractLambdaWrapper<T, Children>>
        extends AbstractWrapper<T, SFunction<T, ?>, Children> {

}
