package com.easy.mongodb.core.biz;

import com.easy.mongodb.common.annotation.TableField;
import com.easy.mongodb.common.enums.FieldStrategy;
import com.easy.mongodb.common.enums.FieldType;
import com.easy.mongodb.core.config.GlobalConfig;
import lombok.Data;

import java.lang.reflect.Field;

/**
 * @ProjectName: easy-mongodb
 * @Description: 实体字段信息
 * @Author: vapeshop
 * @Date: 2022/6/20 11:03:21
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 11:03:21
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
public class TableFieldInfo {
    /**
     * 字段策略 默认，自判断 null
     */
    private final FieldStrategy fieldStrategy;
    /**
     * 忽略的字段
     */
    private Boolean ignoreColumn;
    /**
     * 实体类字段名
     */
    private String column;
    /**
     * 字段类型,如Integer
     */
    private String columnType;
    /**
     * mongodb中的字段名
     */
    private String mappingColumn;
    /**
     * 自动在mongodb中的存储类型
     */
    private FieldType fieldType;
    /**
     * 用户配置的日期格式 例如yyyy-MM-dd HH:mm:ss
     */
    private String dateFormat;
    /**
     * 存在 TableField 且包含JoinCondition注解时
     */
    private JoinConditionDescription description;


    /**
     * 存在 TableField 注解时, 使用的构造函数
     *
     * @param dbConfig   索引配置
     * @param field      字段
     * @param tableField 字段注解
     */
    public TableFieldInfo(GlobalConfig.DbConfig dbConfig, Field field, TableField tableField) {
        this.column = field.getName();

        // 优先使用单个字段注解，否则使用全局配置
        if (tableField.strategy() == FieldStrategy.DEFAULT) {
            this.fieldStrategy = dbConfig.getFieldStrategy();
        } else {
            this.fieldStrategy = tableField.strategy();
        }
    }

    /**
     * 不存在 TableField 注解时, 使用的构造函数
     *
     * @param dbConfig 索引配置
     * @param field    字段
     */
    public TableFieldInfo(GlobalConfig.DbConfig dbConfig, Field field) {
        this.fieldStrategy = dbConfig.getFieldStrategy();
//        System.out.println(field.getType());
        this.column = field.getName();
    }

    @Override
    public String toString() {
        return "{" +
                "fieldStrategy:" + this.fieldStrategy +
                ", ignoreColumn:'" + this.ignoreColumn + '\'' +
                ", column:'" + this.column + '\'' +
                ", columnType:'" + this.columnType + '\'' +
                ", mappingColumn:'" + this.mappingColumn + '\'' +
                ", fieldType:" + this.fieldType +
                ", dateFormat:'" + this.dateFormat + '\'' +
//                ", parentName:'" + parentName + '\'' +
//                ", childName:'" + childName +
                '}';
    }
}
