package com.easy.mongodb.core.toolkit;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.core.toolkit
 * @Description: 主键生成器
 * @Author: vapeshop
 * @Date: 2022/6/25 17:48
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/25 17:48
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class IdGenerator {
    private static Sequence WORKER = new Sequence();

    public IdGenerator() {
    }

    public static long getId() {
        return WORKER.nextId();
    }

    public static String getIdStr() {
        return String.valueOf(WORKER.nextId());
    }
}
