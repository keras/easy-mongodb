package com.easy.mongodb.starter.config;

import com.easy.mongodb.core.config.GlobalConfig;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @ProjectName: easy-mongodb
 * @Description: /easy-mongodb基础配置项
 * @Author: vapeshop
 * @Date: 2022/6/15 20:37:14
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/15 20:37:14
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
@Configuration
@ConfigurationProperties(value = "easy-mongodb")
@ConditionalOnProperty(prefix = "easy-mongodb", name = {"enable"}, havingValue = "true", matchIfMissing = true)
public class EasyMongoDBConfigProperties {
    /**
     * 是否开启easy-mongodb 默认开启
     */
    private boolean enable = true;
    /**
     * mongodb address mongodb地址
     */
    private List<String> address;
    /**
     * username of mongodb 用户名,可缺省
     */
    private String username;
    /**
     * password of mongodb 密码,可缺省
     */
    private String password;

    private Integer maxWaitTime;
    private String authDB;
    /**
     * global config 全局配置
     */
    @NestedConfigurationProperty
    private GlobalConfig globalConfig = new GlobalConfig();
    /**
     * Specifies the minimum number of connections that must exist at any moment in a single connection pool.
     * Default: 0
     **/
    private Integer minPoolSize;

    /**
     * Specifies the maximum number of connections that a connection pool may have at a given time.
     * Default: 100
     **/
    private Integer maxPoolSize;
    /**
     * Specifies the maximum amount of time, in milliseconds that a thread may wait for a connection to become
     * available.
     * <p>
     * Default: 120000 (120 seconds)
     **/
    private Integer waitQueueTimeoutMS;
    /**
     * Specifies the maximum amount of time, in milliseconds, the driver will wait for server selection to succeed
     * before throwing an exception.
     * <p>
     * Default: 30000 (30 seconds)
     **/
    private Integer serverSelectionTimeoutMS;
    /**
     * When communicating with multiple instances of MongoDB in a replica set,
     * the driver will only send requests to a server whose response
     * time is less than or equal to the server with the fastest response time
     * plus the local threshold, in milliseconds.
     * <p>
     * Default: 15
     **/
    private Integer localThresholdMS;
    /**
     * Specifies the frequency, in milliseconds that the driver will wait between attempts to determine the current
     * state of each server in the cluster.
     * <p>
     * Default: 10000 (10 seconds)
     **/
    private Integer heartbeatFrequencyMS;
    /**
     * Specifies that the connection string provided includes multiple hosts. When specified, the driver attempts to
     * find all members of that set.
     * <p>
     * Default: null
     **/
    private String replicaSet;

    /**
     * Specifies that all communication with MongoDB instances should use TLS/SSL. Superseded by the tls option.
     * <p>
     * Default: false
     **/
    private Boolean ssl;
    /**
     * Specifies that all communication with MongoDB instances should use TLS. Supersedes the ssl option.
     * <p>
     * Default: false
     **/
    private Boolean tls;
    /**
     * Specifies that the driver should allow invalid hostnames for TLS connections. Has the same effect as setting
     * tlsAllowInvalidHostnames to true. To configure TLS security constraints in other ways, use a custom SSLContext.
     * <p>
     * Default: false
     **/
    private Boolean tlsInsecure;
    /**
     * Specifies that the driver should allow invalid hostnames in the certificate for TLS connections. Supersedes
     * sslInvalidHostNameAllowed.
     * <p>
     * Default: false
     **/
    private Boolean tlsAllowInvalidHostnames;
    /**
     * Specifies the maximum amount of time, in milliseconds, the Java driver waits for a connection to open before
     * timing out. A value of 0 instructs the driver to never time out while waiting for a connection to open.
     * <p>
     * Default: 10000 (10 seconds)
     **/
    private Integer connectTimeoutMS;
    /**
     * Specifies the maximum amount of time, in milliseconds, the Java driver will wait to send or receive a request
     * before timing out. A value of 0 instructs the driver to never time out while waiting to send or receive a
     * request.
     * <p>
     * Default: 0
     **/
    private Integer socketTimeoutMS;
    /**
     * Specifies the maximum amount of time, in milliseconds, the Java driver will allow a pooled connection to idle
     * before closing the connection. A value of 0 indicates that there is no upper bound on how long the driver can
     * allow a pooled collection to be idle.
     * <p>
     * Default: 0
     **/
    private Integer maxIdleTimeMS;
    /**
     * Specifies the maximum amount of time, in milliseconds, the Java driver will continue to use a pooled connection
     * before closing the connection. A value of 0 indicates that there is no upper bound on how long the driver can
     * keep a pooled connection open.
     * <p>
     * Default: 0
     **/
    private Integer maxLifeTimeMS;
    /**
     * Specifies that the driver must wait for the connected MongoDB instance to group commit to the journal file on
     * disk for all writes.
     * <p>
     * Default: false
     **/
    private Boolean journal;
    /**
     * Specifies the write concern. For more information on values, see the server documentation for the w option.
     * <p>
     * Default: 1
     **/
    private Integer w;
    /**
     * Specifies a time limit, in milliseconds, for the write concern. For more information, see the server
     * documentation for the wtimeoutMS option. A value of 0 instructs the driver to never time out write operations.
     * <p>
     * Default: 0
     **/
    private Integer wtimeoutMS;
    /**
     * Specifies the read preference.
     * For more information on values, see the server documentation for the
     * readPreference option.
     * <p>
     * Default: primary
     * primary 主节点，默认模式，读操作只在主节点，如果主节点不可用，报错或者抛出异常。
     * primaryPreferred 首选主节点，大多情况下读操作在主节点，如果主节点不可用，如故障转移，读操作在从节点。
     * secondary 从节点，读操作只在从节点， 如果从节点不可用，报错或者抛出异常。
     * secondaryPreferred 首选从节点，大多情况下读操作在从节点，特殊情况（如单主节点架构）读操作在主节点。
     * nearest 最邻近节点，读操作在最邻近的成员，可能是主节点或者从节点。 MongoDB 写入和事务处理只能在主节点
     **/
    private String readPreference;
    /**
     * Specifies the read preference tags. For more information on values, see the server documentation for the
     * readPreferenceTags option.
     * <p>
     * Default: null
     **/
    private String readPreferenceTags;
    /**
     * Specifies, in seconds, how stale a secondary can be before the driver stops communicating with that secondary.
     * The minimum value is either 90 seconds or the heartbeat frequency plus 10 seconds, whichever is greater. For more
     * information, see the server documentation for the maxStalenessSeconds option. Not providing a parameter or
     * explicitly specifying -1 indicates that there should be no staleness check for secondaries.
     * <p>
     * Default: -1
     **/
    private Integer maxStalenessSeconds;
    /**
     * Specifies the authentication mechanism that the driver should use if a credential was supplied.
     * <p>
     * Default: By default, the client picks the most secure mechanism available based on the server version. For
     * possible values, see the server documentation for the authMechanism option.
     **/
    private String authMechanism;
    /**
     * Specifies the database that the supplied credentials should be validated against.
     * <p>
     * Default: admin
     **/
    private String authSource;
    /**
     * Specifies authentication properties for the specified authentication mechanism as a list of colon-separated
     * properties and values. For more information, see the server documentation for the authMechanismProperties
     * option.
     * <p>
     * Default: null
     **/
    private String authMechanismProperties;
    /**
     * Specifies the name of the application provided to MongoDB instances during the connection handshake. Can be used
     * for server logs and profiling.
     * <p>
     * Default: null
     **/
    private String appName;
    /**
     * Specifies one or more compression algorithms that the driver will attempt to use to compress requests sent to the
     * connected MongoDB instance. Possible values include: zlib, snappy, and zstd.
     * <p>
     * Default: null
     **/
    private String compressors;
    /**
     * Specifies the degree of compression that
     * Zlib
     * should use to decrease the size of requests to the connected MongoDB instance. The level can range from -1 to 9,
     * with lower values compressing faster (but resulting in larger requests) and larger values compressing slower (but
     * resulting in smaller requests).
     * <p>
     * Default: null
     **/
    private Integer zlibCompressionLevel;
    /**
     * Specifies that the driver must retry supported write operations if they fail due to a network error.
     * <p>
     * Default: true
     **/
    private Boolean retryWrites;
    /**
     * Specifies that the driver must retry supported read operations if they fail due to a network error.
     * <p>
     * Default: true
     **/
    private Boolean retryReads;
    /**
     * Specifies the UUID representation to use for read and write operations. For more information, see the driver
     * documentation for the
     * MongoClientSettings.getUuidRepresentation() method
     * .
     * <p>
     * Default: unspecified
     **/
    private String uuidRepresentation;
    /**
     * Specifies that the driver must connect to the host directly.
     * <p>
     * Default: false
     **/
    private Boolean directConnection;
    /**
     * Specifies the maximum number of connections a pool may be establishing concurrently.
     * <p>
     * Default: 2
     **/
    private Integer maxConnecting;
    /**
     * Specifies the service name of the
     * SRV resource records
     * the driver retrieves to construct your seed list. You must use the DNS Seed List Connection Format in your
     * connection URI to use this option.
     * <p>
     * Default: mongodb
     **/
    private String srvServiceName;

}
