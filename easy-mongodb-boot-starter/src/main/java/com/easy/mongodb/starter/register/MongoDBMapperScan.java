package com.easy.mongodb.starter.register;


import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * ProjectName: easy-mongodb
 * Description: 全局Mapper扫描注解
 * Author: vapeshop
 * Date: 2022/6/20 15:16:52
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 15:16:52
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(MapperScannerRegister.class)
public @interface MongoDBMapperScan {
    String value();
}
