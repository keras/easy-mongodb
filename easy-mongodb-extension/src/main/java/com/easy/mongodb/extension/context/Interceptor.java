package com.easy.mongodb.extension.context;

import com.easy.mongodb.extension.plugins.Plugin;


/**
 * @ProjectName: easy-mongodb
 * @Description: Interceptor
 * @Author: vapeshop
 * @Date: 2022/6/20 09:15:35
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 09:15:35
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface Interceptor {

    Object intercept(Invocation invocation) throws Throwable;

    /**
     * 代理
     *
     * @param t   泛型
     * @param <T> 泛型
     * @return 泛型
     */
    default <T> T plugin(T t) {
        return Plugin.wrap(t, this);
    }
}
