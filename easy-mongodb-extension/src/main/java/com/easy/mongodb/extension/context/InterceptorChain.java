package com.easy.mongodb.extension.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ProjectName: easy-mongodb
 * @Description: 拦截器链
 * @Author: vapeshop
 * @Date: 2022/6/20 09:18:15
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 09:18:15
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class InterceptorChain {

    /**
     * 拦截器集合
     */
    private final List<Interceptor> interceptors = new ArrayList<>();

    /**
     * 装载拦截器
     *
     * @param t   泛型
     * @param <T> 泛型
     * @return 泛型
     */
    public <T> T pluginAll(T t) {
        for (Interceptor interceptor : interceptors) {
            t = interceptor.plugin(t);
        }
        return t;
    }

    /**
     * 添加拦截器
     *
     * @param interceptor 拦截器
     */
    public void addInterceptor(Interceptor interceptor) {
        interceptors.add(interceptor);
    }


    /**
     * 获取所有拦截器
     *
     * @return 拦截器集合
     */
    public List<Interceptor> getInterceptors() {
        return Collections.unmodifiableList(interceptors);
    }
}
