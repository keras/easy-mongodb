package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysLogininfor;


/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 系统访问日志情况信息 数据层
 * Author: vapeshop
 * Date: 2022/7/12 16:51:57
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 16:51:57
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysLogininforMapper extends BaseMongoMapper<SysLogininfor> {
}
