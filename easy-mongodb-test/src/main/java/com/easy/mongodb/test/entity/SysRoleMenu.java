package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;
import org.bson.types.ObjectId;

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author ruoyi
 */
@Table(value = "sys_role_menu")
public class SysRoleMenu {
    /** 角色ID */
    private ObjectId roleId;

    /** 菜单ID */
    private ObjectId menuId;

    public ObjectId getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = new ObjectId(roleId);
    }

    public ObjectId getMenuId() {
        return this.menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = new ObjectId(menuId);
    }

    @Override
    public String toString() {
        return "SysRoleMenu{" +
                "roleId=" + this.roleId +
                ", menuId=" + this.menuId +
                '}';
    }
}
