package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.JoinCondition;
import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.annotation.TableField;
import com.easy.mongodb.common.annotation.TableId;
import com.easy.mongodb.common.enums.FieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Set;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.sample.entity
 * @Description: 用户信息
 * @Author: vapeshop
 * @Date: 2022/6/28 10:45
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/28 10:45
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table(value = "sys_user")
public class User {
    @TableId
    private String id;
    @TableField("user_id")
    private String userId;
    private String username;

    private String password;
    private Integer age;
    // 关联身份证完整信息
    // 其中‘conditions’的赋值，表示当前对象(User)的id属性与关联对象(IdentityCard)的userId属性相同
    // 即，如果当前User对象的id为1的话，那么IdentityCard数据中某条userId为1的数据会被关联过来
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    selfField = "user_id", joinField = "user_id"))
    private IdentityCard idCard;

    // 同理只关联身份证号码，@BindField比@BindEntity多了entity、field字段。
    // 因为无法从属性的类型上直接获取实体，需要指明哪个实体的哪个字段。
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    entity = IdentityCard.class, field = "identity_number",
                    selfField = "user_id", joinField = "user_id"))
    private String idCardNumber;
    // 通过中间表关联所有相关的角色。
    // 通过中间表的形式需要使用@Bind*ByMid
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    entity = Role.class,
                    midEntity = UserRole.class, selfField = "user_id", selfMidField = "user_id", joinMidField = "role_id", joinField = "role_id"
            ))
    private List<Role> roles;
    // 关联该用户发布的所有文章（"audit = 1" 表示的是Article下的audit为1的情况，customCondition的值只能是被关联表下的字段值，且会以and的形式添加在查询条件末尾。）
//    @TableField(
//            fieldType = FieldType.JOIN,
//            condition = @JoinCondition(
//                    entity = Article.class,
//                    selfField = "id", joinField = "publishedUserId"
//            ))
    private List<Article> articles;

    private Set<Faq> faqs;

    public User(String username, Integer age, Set<Faq> faqs) {
        this.username = username;
        this.age = age;
        this.faqs = faqs;
    }
}
