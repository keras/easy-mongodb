package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;
import lombok.Data;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.test.entity
 * @Description: 用户-角色关联关系
 * @Author: vapeshop
 * @Date: 2022/6/28 10:39
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/28 10:39
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
@Table(value = "sys_user_role")
public class UserRole {
    private String id;

    private String userId;

    private String roleId;
}
