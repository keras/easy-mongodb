package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;

/**
 * 通知公告表 sys_notice
 *
 * @author ruoyi
 */
@Table(value = "sys_notice")
public class SysNotice extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /** 公告标题 */
    private String noticeTitle;

    /** 公告类型（1通知 2公告） */
    private String noticeType;

    /** 公告内容 */
    private String noticeContent;

    /** 公告状态（0正常 1关闭） */
    private String status;

//    public Long getNoticeId() {
//        return this.noticeId;
//    }
//
//    public void setNoticeId(Long noticeId) {
//        this.noticeId = noticeId;
//    }

    public String getNoticeTitle() {
        return this.noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeType() {
        return this.noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getNoticeContent() {
        return this.noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysNotice{" +
                "noticeTitle='" + this.noticeTitle + '\'' +
                ", noticeType='" + this.noticeType + '\'' +
                ", noticeContent='" + this.noticeContent + '\'' +
                ", status='" + this.status + '\'' +
                '}';
    }
}
