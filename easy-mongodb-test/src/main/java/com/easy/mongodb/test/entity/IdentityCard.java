package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.TableId;
import lombok.Data;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.test.entity
 * @Description: 身份证信息
 * @Author: vapeshop
 * @Date: 2022/6/28 09:15
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/28 09:15
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
public class IdentityCard {
    @TableId
    private String id;

    private String userId;

    private String identityNumber;
}
