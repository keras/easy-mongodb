package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysUserRole;

import java.util.List;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 用户与角色关联表 数据层
 * Author: vapeshop
 * Date: 2022/7/12 15:53:13
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:53:13
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysUserRoleMapper extends BaseMongoMapper<SysUserRole> {
    /**
     * 通过用户ID删除用户和角色关联
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserRoleByUserId(Long userId);

    /**
     * 批量删除用户和角色关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserRole(Long[] ids);

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int countUserRoleByRoleId(Long roleId);

    /**
     * 批量新增用户角色信息
     *
     * @param userRoleList 用户角色列表
     * @return 结果
     */
    public int batchUserRole(List<SysUserRole> userRoleList);

    /**
     * 删除用户和角色关联信息
     *
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    public int deleteUserRoleInfo(SysUserRole userRole);

    /**
     * 批量取消授权用户角色
     *
     * @param roleId  角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public int deleteUserRoleInfos(Long roleId, Long[] userIds);
}
