package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.IdentityCard;

/**
 * ProductName: easy-mongodb
 * Package: com.easy.mongodb.test.mapper
 * Description:
 * Author: vapeshop
 * Date: 2022/7/8 10:38
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/8 10:38
 * UpdateRemark: The modified content
 * Version: 1.0.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface IdentityCardMapper extends BaseMongoMapper<IdentityCard> {
}
