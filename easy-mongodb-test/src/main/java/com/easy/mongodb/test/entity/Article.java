package com.easy.mongodb.test.entity;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.sample.entity
 * @Description: 文章
 * @Author: vapeshop
 * @Date: 2022/6/28 10:46
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/28 10:46
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class Article {
    private String id;

    private String title;

    private String content;

    private String publishedUserId;

    private int audit;

    private Long publishedTime;
}
