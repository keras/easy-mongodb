package com.easy.mongodb.test.entity;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.test.entity
 * @Description: 角色-菜单关联关系
 * @Author: vapeshop
 * @Date: 2022/6/28 10:38
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/28 10:38
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class RoleMenu {
    private String id;

    private String roleId;

    private String menuId;
}
