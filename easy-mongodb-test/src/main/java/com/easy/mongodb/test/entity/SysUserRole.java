package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Index;
import com.easy.mongodb.common.annotation.IndexField;
import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.enums.SortDirectionEnum;
import org.bson.types.ObjectId;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 用户和角色关联 sys_user_role
 * Author: vapeshop
 * Date: 2022/7/14 13:48:42
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/14 13:48:42
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Table(value = "sys_user_role", indexs = {
        @Index(
                fields = {
                        @IndexField(name = "userId", direction = SortDirectionEnum.ASC),
                        @IndexField(name = "roleId", direction = SortDirectionEnum.ASC)}
        )})
public class SysUserRole {
    /** 用户ID */
    private ObjectId userId;

    /** 角色ID */
    private ObjectId roleId;

    public ObjectId getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = new ObjectId(userId);
    }

    public ObjectId getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = new ObjectId(roleId);
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
                "userId=" + this.userId +
                ", roleId=" + this.roleId +
                '}';
    }
}
