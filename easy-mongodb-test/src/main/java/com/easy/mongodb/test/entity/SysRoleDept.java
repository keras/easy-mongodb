package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;
import org.bson.types.ObjectId;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 角色和部门关联 sys_role_dept
 * Author: vapeshop
 * Date: 2022/7/14 13:56:08
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/14 13:56:08
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Table(value = "sys_role_dept")
public class SysRoleDept {
    /** 角色ID */
    private ObjectId roleId;

    /** 部门ID */
    private ObjectId deptId;

    public ObjectId getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = new ObjectId(roleId);
    }

    public ObjectId getDeptId() {
        return this.deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = new ObjectId(deptId);
    }

    @Override
    public String toString() {
        return "SysRoleDept{" +
                "roleId=" + this.roleId +
                ", deptId=" + this.deptId +
                '}';
    }
}
