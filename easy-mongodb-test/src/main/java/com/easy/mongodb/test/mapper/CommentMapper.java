package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.Comment;

/**
 * 父子类型-子文档的mapper
 * <p>
 * Copyright © 2022 xpc1024 All Rights Reserved
 **/
public interface CommentMapper extends BaseMongoMapper<Comment> {
}
