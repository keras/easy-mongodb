package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author ruoyi
 */
public interface SysOperLogMapper extends BaseMongoMapper<SysOperLog> {
}
