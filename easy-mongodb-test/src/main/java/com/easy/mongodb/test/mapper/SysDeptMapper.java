package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysDept;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 部门管理 数据层
 * Author: vapeshop
 * Date: 2022/7/12 15:50:45
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:50:45
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysDeptMapper extends BaseMongoMapper<SysDept> {
//    /**
//     * 查询部门管理数据
//     *
//     * @param dept 部门信息
//     * @return 部门信息集合
//     */
//    public List<SysDept> selectDeptList(SysDept dept);
//
//    /**
//     * 根据角色ID查询部门树信息
//     *
//     * @param roleId            角色ID
//     * @param deptCheckStrictly 部门树选择项是否关联显示
//     * @return 选中部门列表
//     */
//    public List<Long> selectDeptListByRoleId(Long roleId, boolean deptCheckStrictly);
//
//    /**
//     * 根据部门ID查询信息
//     *
//     * @param deptId 部门ID
//     * @return 部门信息
//     */
//    public SysDept selectDeptById(Long deptId);
//
//    /**
//     * 根据ID查询所有子部门
//     *
//     * @param deptId 部门ID
//     * @return 部门列表
//     */
//    public List<SysDept> selectChildrenDeptById(Long deptId);
//
//    /**
//     * 根据ID查询所有子部门（正常状态）
//     *
//     * @param deptId 部门ID
//     * @return 子部门数
//     */
//    public int selectNormalChildrenDeptById(Long deptId);
//
//    /**
//     * 是否存在子节点
//     *
//     * @param deptId 部门ID
//     * @return 结果
//     */
//    public int hasChildByDeptId(Long deptId);
//
//    /**
//     * 查询部门是否存在用户
//     *
//     * @param deptId 部门ID
//     * @return 结果
//     */
//    public int checkDeptExistUser(Long deptId);
//
//    /**
//     * 校验部门名称是否唯一
//     *
//     * @param deptName 部门名称
//     * @param parentId 父部门ID
//     * @return 结果
//     */
//    public SysDept checkDeptNameUnique(String deptName, Long parentId);
//
//    /**
//     * 新增部门信息
//     *
//     * @param dept 部门信息
//     * @return 结果
//     */
//    public int insertDept(SysDept dept);
//
//    /**
//     * 修改部门信息
//     *
//     * @param dept 部门信息
//     * @return 结果
//     */
//    public int updateDept(SysDept dept);
//
//    /**
//     * 修改所在部门正常状态
//     *
//     * @param deptIds 部门ID组
//     */
//    public void updateDeptStatusNormal(Long[] deptIds);
//
//    /**
//     * 修改子元素关系
//     *
//     * @param depts 子元素
//     * @return 结果
//     */
//    public int updateDeptChildren(List<SysDept> depts);
//
//    /**
//     * 删除部门管理信息
//     *
//     * @param deptId 部门ID
//     * @return 结果
//     */
//    public int deleteDeptById(Long deptId);
}
