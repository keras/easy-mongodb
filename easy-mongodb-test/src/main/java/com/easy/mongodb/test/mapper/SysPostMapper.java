package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysPost;


/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 岗位信息 数据层
 * Author: vapeshop
 * Date: 2022/7/12 15:59:47
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:59:47
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysPostMapper extends BaseMongoMapper<SysPost> {
}
