package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 字典数据表 sys_dict_data
 * Author: vapeshop
 * Date: 2022/7/20 15:51:56
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/20 15:51:56
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Table(value = "sys_dict_data")
public class SysDictData extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 字典编码 */
    private Long dictCode;

    /** 字典排序 */
    private Long dictSort;

    /** 字典标签 */
    private String dictLabel;

    /** 字典键值 */
    private String dictValue;

    /** 字典类型 */
    private String dictType;

    /** 样式属性（其他样式扩展） */
    private String cssClass;

    /** 表格字典样式 */
    private String listClass;

    /** 是否默认（Y是 N否） */
    private String isDefault;

    /** 状态（0正常 1停用） */
    private String status;

    public Long getDictCode() {
        return this.dictCode;
    }

    public void setDictCode(Long dictCode) {
        this.dictCode = dictCode;
    }

    public Long getDictSort() {
        return this.dictSort;
    }

    public void setDictSort(Long dictSort) {
        this.dictSort = dictSort;
    }

    public String getDictLabel() {
        return this.dictLabel;
    }

    public void setDictLabel(String dictLabel) {
        this.dictLabel = dictLabel;
    }

    public String getDictValue() {
        return this.dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getDictType() {
        return this.dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getCssClass() {
        return this.cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getListClass() {
        return this.listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }


    public String getIsDefault() {
        return this.isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysDictData{" +
                "dictCode=" + this.dictCode +
                ", dictSort=" + this.dictSort +
                ", dictLabel='" + this.dictLabel + '\'' +
                ", dictValue='" + this.dictValue + '\'' +
                ", dictType='" + this.dictType + '\'' +
                ", cssClass='" + this.cssClass + '\'' +
                ", listClass='" + this.listClass + '\'' +
                ", isDefault='" + this.isDefault + '\'' +
                ", status='" + this.status + '\'' +
                '}';
    }
}
