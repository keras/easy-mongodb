package com.easy.mongodb.test.delete;

import com.easy.mongodb.core.conditions.query.LbqWrapper;
import com.easy.mongodb.test.TestEasyMongoDBApplication;
import com.easy.mongodb.test.entity.Document;
import com.easy.mongodb.test.mapper.DocumentMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 删除测试
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestEasyMongoDBApplication.class)
public class DeleteTest {
    @Resource
    private DocumentMapper documentMapper;

    @Test
    public void testDelete() {
        // 测试删除数据 删除有两种情况:根据id删或根据条件删
        // 鉴于根据id删过于简单,这里仅演示根据条件删,以老李的名义删,让老李心理平衡些
        LbqWrapper<Document> wrapper = new LbqWrapper<>();
        String title = "老李";
        wrapper.eq(Document::getTitle, title);
        long successCount = this.documentMapper.delete(wrapper);
        System.out.println(successCount);
    }

    @Test
    public void testDeleteById() {
        long successCount = this.documentMapper.deleteById("7");
        System.out.println(successCount);
    }


    @Test
    public void testDeleteBatchIds() throws IOException {
        List<String> ids = Arrays.asList("62cfe8c44895a216ab7960d2", "62cfe8c44895a216ab7960d3", "62cfe8c44895a216ab7960d4");
        long successCount = this.documentMapper.deleteBatchIds(ids);
        System.out.println(successCount);
        LbqWrapper<Document> wrapper = new LbqWrapper<>();
        wrapper.isNotNull(Document::getTitle)
                .and(w -> w.eq(Document::getCustomField, "乌拉").or(e -> w.eq(Document::getCustomField, "魔鬼")));
        successCount = this.documentMapper.delete(wrapper);
        System.out.println(successCount);
    }
}
