package com.easy.mongodb.test;

import com.easy.mongodb.starter.register.MongoDBMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
@SpringBootApplication
@MongoDBMapperScan("com.easy.mongodb.test.mapper")
public class TestEasyMongoDBApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestEasyMongoDBApplication.class, args);
    }
}
