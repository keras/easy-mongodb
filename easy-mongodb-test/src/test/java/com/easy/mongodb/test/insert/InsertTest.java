package com.easy.mongodb.test.insert;

import com.easy.mongodb.test.TestEasyMongoDBApplication;
import com.easy.mongodb.test.entity.Document;
import com.easy.mongodb.test.mapper.DocumentMapper;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 插入测试
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestEasyMongoDBApplication.class)
public class InsertTest {

    @Resource
    private DocumentMapper documentMapper;


    @Test
    public void testInsert() {

//        this.documentMapper.drop();
        // 测试插入数据
        Document document = new Document();
        document.setId("5");
        document.setTitle("老汉");
        document.setContent("人才");
        document.setCreator("吃饭");
        document.setLocation("40.171975,116.587105");
        document.setGmtCreate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        document.setCustomField("俄罗斯方块");
        Point point = new Point(new Position(13.400544, 52.530286));
        document.setGeoLocation("13.400544, 52.530286");
        document.setStarNum(1);
        long successCount = this.documentMapper.insert(document);
        Assert.assertEquals(successCount, 1l);
    }


    @Test
    public void testInsertBatch() {
        List<Document> documentList = new ArrayList<>();
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Document document = new Document();
        document.setId("2");
        document.setTitle("老汉");
        document.setContent("推*技术过硬");
        document.setCreator("隔壁老王");


        document.setGmtCreate(now);
        document.setCustomField("乌拉巴拉小魔仙");
        document.setLocation("40.17836693398477,116.64002551005981");

        Document document1 = new Document();
        document1.setId("3");
        document1.setTitle("老王");
        document1.setContent("推*技术过硬");
        document1.setCreator("隔壁老王");
        document1.setGmtCreate(now);
        document1.setCustomField("魔鬼的步伐");
        document1.setLocation("40.19103839805197,116.5624013764374");


        Document document2 = new Document();
        document2.setId("4");
        document2.setTitle("老李");
        document2.setContent("推*技术过硬");
        document2.setCreator("大猪蹄子");
        document2.setGmtCreate(now);
        document2.setCustomField("锤子科技");
        document2.setLocation("40.13933715136454,116.63441990026217");

        documentList.add(document);
        documentList.add(document1);
        documentList.add(document2);

        Long successCount = this.documentMapper.insertBatch(documentList);
        System.out.println(successCount);
        // id可以直接从被插入的数据中取出
        documentList.forEach(System.out::println);
    }
}
