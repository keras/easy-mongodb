package com.easy.mongodb.common.enums;

/**
 * ProductName: easy-mongodb
 * Package: com.easy.mongodb.common.enums
 * Description: 索引类型
 * Author: vapeshop
 * Date: 2022/7/11 16:00
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/11 16:00
 * UpdateRemark: The modified content
 * Version: 1.0.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum IndexType {
    /** 单一 */
    DEFAULT,
    /** 单一组合 */
    HASH,
    /** 单一组合 */
    COMPOUND,
    /** 文本 */
    TEXT,
    /**
     * 2d索引只能对点进行索引，并且使用在保存的经纬度字段上。
     * 一级字段可以不用加引号。不能对平面上的线和面进行2d索引
     */
    GEO2D,
    /**
     * 2dsphere索引用于地球表面类型的地图，允许使用在 Legacy Coordinate Pairs 保存的经纬度字段上和使用GeoJSON格式保存的点、线和多边形字段上。
     * <p>
     * 2dsphere索引只支持球形查询（即球面上几何图形的查询）
     * <p>
     * 2dsphere可以对球形的点，线，面，多点，多线，多面，geometryCollection进行索引，但是要注意的是，索引字段为geometry这种一级字段，不要用geometry.coordinates这种二级字段
     */
    GEO2D_SPHERE;
}
