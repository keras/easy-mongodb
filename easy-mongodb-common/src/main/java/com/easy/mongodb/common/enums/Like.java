package com.easy.mongodb.common.enums;

/**
 * ProjectName: easy-mongodb
 * Description: SQL like 枚举
 * Author: vapeshop
 * Date: 2022/7/12 13:56:56
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 13:56:56
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum Like {
    /**
     * %值
     */
    LEFT,
    /**
     * 值%
     */
    RIGHT,
    /**
     * %值%
     */
    DEFAULT
}
