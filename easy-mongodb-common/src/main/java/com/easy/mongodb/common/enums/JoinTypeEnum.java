package com.easy.mongodb.common.enums;

/**
 * ProjectName: easy-mongodb
 * Description: 嵌套及父子类型枚举
 * Author: vapeshop
 * Date: 2022/6/20 14:11:10
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:11:10
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum JoinTypeEnum {
    NESTED,
    /**
     * 中间表
     **/
    STAGING;
}
