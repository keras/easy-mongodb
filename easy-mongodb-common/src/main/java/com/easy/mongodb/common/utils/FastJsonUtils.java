package com.easy.mongodb.common.utils;

import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * ProjectName: easy-mongodb
 * Description: fastjson 工具类
 * Author: vapeshop
 * Date: 2022/6/20 14:14:37
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:14:37
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FastJsonUtils {
    /**
     * 设置fastjson toJsonString字段
     *
     * @param clazz  类
     * @param fields 字段列表
     * @return 前置过滤器
     */
    public static SimplePropertyPreFilter getSimplePropertyPreFilter(Class<?> clazz, Set<String> fields) {
        if (CollectionUtils.isEmpty(fields)) {
            return null;
        }
        SimplePropertyPreFilter simplePropertyPreFilter = new SimplePropertyPreFilter(clazz);
        fields.forEach(field -> simplePropertyPreFilter.getExcludes().add(field));
        return simplePropertyPreFilter;
    }
}
