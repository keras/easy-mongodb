package com.easy.mongodb.common.annotation;

import com.easy.mongodb.common.enums.FieldFill;
import com.easy.mongodb.common.enums.FieldStrategy;
import com.easy.mongodb.common.enums.FieldType;
import com.easy.mongodb.common.params.DefaultNestedClass;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ProjectName: easy-mongodb
 * @Description: 字段注解
 * @Author: vapeshop
 * @Date: 2022/6/20 11:04:04
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 11:04:04
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface TableField {
    FieldFill fill() default FieldFill.DEFAULT;

    /**
     * 自定义字段在mongo中的名称
     *
     * @return 字段在mongo中的名称
     */
    String value() default "";

    /**
     * 是否为数据库表字段 默认 true 存在，false 不存在
     *
     * @return 存在
     */
    boolean exist() default true;

    /**
     * 字段在集合中的类型,建议根据业务场景指定,若不指定则由本框架自动推断
     *
     * @return 类型
     */
    FieldType fieldType() default FieldType.NONE;

    /**
     * 字段验证策略
     *
     * @return 默认策略
     */
    FieldStrategy strategy() default FieldStrategy.DEFAULT;

    /**
     * 集合中的日期格式
     *
     * @return 日期格式 例如yyyy-MM-dd HH:mm:ss
     */
    String dateFormat() default "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 默认嵌套类
     *
     * @return 默认嵌套类
     */
    Class<?> nestedClass() default DefaultNestedClass.class;

    /**
     * ---------------> start join
     * 关联Entity所需要的条件
     */
    JoinCondition condition() default @JoinCondition;
}
