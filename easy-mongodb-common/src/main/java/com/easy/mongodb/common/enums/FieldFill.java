package com.easy.mongodb.common.enums;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.common.enums
 * @Description:
 * @Author: vapeshop
 * @Date: 2022/6/22 16:48
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/22 16:48
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum FieldFill {
    DEFAULT,
    INSERT,
    UPDATE,
    INSERT_UPDATE;

    private FieldFill() {
    }
}
