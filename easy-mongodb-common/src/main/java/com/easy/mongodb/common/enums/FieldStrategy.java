package com.easy.mongodb.common.enums;

/**
 * ProjectName: easy-mongodb
 * Description: 字段策略枚举
 * Author: vapeshop
 * Date: 2022/6/20 14:12:16
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:12:16
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum FieldStrategy {
    /**
     * 忽略判断
     */
    IGNORED,
    /**
     * 非NULL判断
     */
    NOT_NULL,
    /**
     * 非空判断
     */
    NOT_EMPTY,
    /**
     * 默认的,一般只用于注解里
     */
    DEFAULT;
}
