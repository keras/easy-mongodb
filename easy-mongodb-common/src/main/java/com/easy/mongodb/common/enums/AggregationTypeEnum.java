package com.easy.mongodb.common.enums;

/**
 * ProjectName: easy-mongodb
 * Description: 聚合枚举
 * Author: vapeshop
 * Date: 2022/6/20 14:08:29
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:08:29
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum AggregationTypeEnum {
    /**
     * 求均值
     */
    AVG,
    /**
     * 求最小值
     */
    MIN,
    /**
     * 求最大值
     */
    MAX,
    /**
     * 求和
     */
    SUM,
    /**
     * 按字段分组,相当于mysql group by
     */
    TERMS;
}
