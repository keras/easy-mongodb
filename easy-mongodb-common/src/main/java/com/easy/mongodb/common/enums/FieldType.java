package com.easy.mongodb.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * ProjectName: easy-mongodb
 * Description: 数据类型枚举
 * Author: vapeshop
 * Date: 2022/6/20 14:11:55
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:11:55
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@AllArgsConstructor
public enum FieldType {
    /**
     * none Required inside the framework, do not use 框架内部需要,切勿使用,若不慎使用则会被当做keyword类型
     */
    NONE("none"),
    /**
     * core
     */
    BYTE("byte"),
    SHORT("short"),
    INTEGER("integer"),
    LONG("long"),
    FLOAT("float"),
    DOUBLE("double"),
    HALF_FLOAT("half_float"),
    SCALED_FLOAT("scaled_float"),
    BOOLEAN("boolean"),
    DATE("date"),
    RANGE("range"),
    BINARY("binary"),
    KEYWORD("keyword"),
    TEXT("text"),
    OBJECT_ID("objectId"),
    /**
     * mix
     */
    ARRAY("array"),
    NESTED("nested"),
    JOIN("join"),
    /**
     * geo
     */
    GEO_POINT("geo_point"),
    GEO_SHAPE("geo_shape");
    @Getter
    private String type;


}
