package com.easy.mongodb.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ProjectName: easy-mongodb
 * Description: 主键注解
 * Author: vapeshop
 * Date: 2022/6/20 13:20:13
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 13:20:13
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TableId {
    /**
     * 字段值
     *
     * @return mongo默认_id
     */
    String value() default "_id";
}
